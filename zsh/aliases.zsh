if [[ -f $(which nvim) ]]; then
  alias vi="nvim"
else
  alias vi="vim"
fi

if [[ $(which ls) ]]; then
  alias la="ls -lah"
fi

if [[ $(which sudo) ]]; then
  alias pls="sudo"
fi

if [[ $(which yum) && $(which flatpak) ]]; then
  alias update="bash ~/dotfiles/scripts/update.sh"
fi

alias pfetch="bash ~/dotfiles/scripts/pfetch.sh"
