sudo echo -e "\n### \nChecking for yum..."
if [[ $(which yum) ]]; then
sudo yum update
fi

echo -e "\n### \nChecking for dnf..."
if [[ $(which dnf) ]]; then
sudo dnf update
fi

echo -e "\n### \nChecking for flatpak..."
if [[ $(which flatpak) ]]; then
sudo flatpak update
fi

clear
