[**Original GitHub Reposity**](https://github.com/solidiquis/dotfiles/)
# Dotfiles

**Terminal emulator**: [Alacritty](https://github.com/alacritty/alacritty)

**Terminal multiplexer**: [Tmux](https://github.com/tmux/tmux/wiki)

**Shell**: [Zsh](https://en.wikipedia.org/wiki/Z_shell)

**Text editor**: [NeoVim](https://github.com/neovim/neovim)

## Zsh rice

<img src="https://github.com/solidiquis/solidiquis/blob/master/assets/1.png?raw=true">


## NeoVim rice

<img src="https://github.com/solidiquis/solidiquis/blob/master/assets/2.png?raw=true">
<img src="https://github.com/solidiquis/solidiquis/blob/master/assets/3.png?raw=true">
<img src="https://github.com/solidiquis/solidiquis/blob/master/assets/4.png?raw=true">
<img src="https://github.com/solidiquis/solidiquis/blob/master/assets/5.png?raw=true">
<img src="https://github.com/solidiquis/solidiquis/blob/master/assets/6.png?raw=true">
